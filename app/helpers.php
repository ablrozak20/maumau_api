<?php

use App\Models\User;
use Carbon\Carbon;

function appResponse($data,$message,$msg="")
{
  $array=[
    'success'=>"00",
    'Email Alerady Exists'=>11,
    'Wrong Email Format'=>12,
    'Missing Parameter'=>20,
    'Email Not Found'=>13,
    'Wrong Password'=>21,
    'User Not Found'=>100,
    'forbidden'=>406,
  ];
  $res['status_code']=$array[$message];
  ($msg != "")?$res['message']=$msg:$res['message']=$message;
  // $res['message']=$message;
  if($message=='success'){
  	$res['data']=$data;
  } 	
  elseif($message=='User Not Found'){
    $res['data']='User Id '.$data->input('user_id').' Not found';
  }
  else{
  	$res['data']=(!empty($data->all()))?[$data->all()]:[];
  } 
  return response()->json($res);
}

function cparam($request,$array=[])
{
   $msg='Missing Parameter (';
   $check=$now=true;
   foreach ($array as $key=>$value) {
      if(empty($request->input($value))) {
        if($now==false) $msg.=',';
         $msg.=$value;
         $check=false;
         $now=false;
      }
      if($key==count($array)-1) $msg.=')';
   }
   if($check==false) return appResponse($request,'Missing Parameter',$msg);
   return $check;
}

function generateToken()
{
  $token = base64_encode(str_random(40));
  $check = User::where('remember_token',$token)->get();
  if(!$check->isEmpty()) return generateToken();
  return $token;
}