<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorkExperience extends Model
{
    protected $table = 'user_work_experiences';
    protected $fillable = ['user_id','job_title','company','job_desc','total_year','start_date','end_date','created_at','updated_at'];
}
