<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostJobRequirement extends Model
{
    protected $table = 'post_job_requirements';
    protected $fillable = ['post_jobs_id','sequence_no','title','created_at','updated_at'];
}
