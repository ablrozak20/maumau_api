<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostJobDescription extends Model
{
    protected $table = 'post_job_descriptions';
    protected $fillable = ['post_jobs_id','sequence_no','title','created_at','updated_at'];
}
