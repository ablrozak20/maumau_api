<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostJob extends Model
{
    protected $table = 'post_jobs';
    protected $fillable = ['user_id','job_title','year_experience','start_salary','end_salary','employment_type','closing_date','status','created_at','updated_at'];

    public function posJobDescriptions(){
    	return $this->hasMany('App\Models\PostJobDescription');
    }
    public function PostJobRequirements(){
    	return $this->hasMany('App\Models\PostJobRequirement');
    }
    
}
