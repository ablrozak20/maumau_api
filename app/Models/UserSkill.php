<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSkill extends Model
{
    protected $table = 'user_skills';
    protected $fillable = ['user_id','title','strength','created_at','updated_at'];
}
