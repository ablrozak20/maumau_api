<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationLevel extends Model
{
    protected $table = 'education_level';
    protected $fillable = ['title','created_at','updated_at'];

    public function educations(){
    	return $this->hasMay('App\Models\UserEducation');
    }
}
