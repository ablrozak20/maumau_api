<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    protected $table = 'user_educations';
    protected $fillable = ['user_id','institute','education_level_id','major','graduation_year','gpa','created_at','updated_at'];

    public function education_level(){
    	return $this->belongsTo('App\Models\EducationLevel','education_level_id');
    }
}
