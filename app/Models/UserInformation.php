<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Spatie\Image\Manipulations;
// use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
// use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
// use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

// class UserInformation extends Model implements HasMedia, HasMediaConversions
class UserInformation extends Model implements HasMedia
{
	// use HasMediaTrait;
	use HasMediaTrait;

    protected $table = 'user_informations';
    protected $fillable = ['user_id','name','dob','phone','address','resume','file_assesment','description','profile_strength','created_at','updated_at'];

    // public function registerMediaConversions(Media $media = null) {
    //     $this->addMediaConversion('square')->crop(Manipulations::CROP_CENTER, 250, 250)->performOnCollections('avatar');
    //     $this->addMediaConversion('thumb')->crop(Manipulations::CROP_CENTER, 300, 250)->performOnCollections('avatar');
    //     $this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 480, 320)->performOnCollections('avatar');
    //     $this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 720, 480)->performOnCollections('avatar');
    // }
}
