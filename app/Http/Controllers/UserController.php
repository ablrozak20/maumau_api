<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserInformation;
use App\Models\UserSkill;
use App\Models\UserWorkExperience;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use DB;
// use \File;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         // $this->middleware('auth');
    }

    public function register(Request $request)
    {
        $name=$request->input('name');
        $email=$request->input('email');
        $phone=$request->input('phone');
        $password=$request->input('password');
        $check=['name','email','phone','password'];
        $checkParam=cparam($request,$check);
        $remember_token=generateToken();
        if($checkParam!==true){
            return $checkParam;
        } 
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return appResponse($request,'Wrong Email Format');
        }
        $checkUser=User::where('email',$email)->get();
        if(!$checkUser->isEmpty()){
            return appResponse($request,'Email Alerady Exists');
        } 
        $user = new User;
        $user->email=$email;
        $user->password=app('hash')->make($password);
        $user->remember_token = $remember_token;
        $user->save();

        $userInfo = new UserInformation;
        $userInfo->user_id = $user->id;
        $userInfo->name = $name;
        $userInfo->phone = $phone;
        $userInfo->save();
        DB::table('model_has_roles')->insert([
            'role_id'=>$request->role,
            'model_id'=>$user->id,
            'model_type'=>'App\Models\User'
        ]);
        $hasRole = DB::table('model_has_roles')->where('model_id',$user->id)->where('model_type','App\Models\User')->first();
        $role = $user->getRoleNames()->first();
        //Get Url
        $getMedia = Media::where('model_id',$user->info->id)->first();
        if($getMedia){
            $mediaUrl = env('APP_URL').'/media/'.$getMedia->id.'/'.$getMedia->file_name;
        }
        else{
             $mediaUrl = env('APP_URL').'/img/avatar.png';
        }
        $data = $this->userDetail($user);
        $data=[[
            "user_id"=>$user->id,
            "role"=>$role,
            "avatar"=>$mediaUrl,
            "name"=>$user->info->name,
            "email"=>$user->email,
            "phone"=>(!empty($user->info->phone))?$user->info->phone:'',
            
        ]];
        return appResponse($data,'success');
    }

    function login(Request $request)
    {
        //Declare Variable
        $email=$request->input('email');
        $password=$request->input('password');
        //
        //Check Parameter
        $checkParam=cparam($request,['email','password']);
        if($checkParam!==true) return $checkParam;
        //
        $user = User::where('email', $request->input('email'))->first();
        if (empty($user)) return appResponse($request,'Email Not Found');
        if(!Hash::check($request->input('password'), $user->password)) return appResponse($request,'Wrong Password');
        $token=generateToken();
        User::where('email', $request->input('email'))->update(['remember_token' => $token]);
        $role = $user->getRoleNames()->first();
        //Get Url
        $getMedia = Media::where('model_id',$user->info->id)->first();
        if($getMedia){
            $mediaUrl = env('APP_URL').'/media/'.$getMedia->id.'/'.$getMedia->file_name;
        }
        else{
             $mediaUrl = env('APP_URL').'/img/avatar.png';
        }
        $data=[[
            "user_id"=>$user->id,
            "role"=>$role,
            "avatar"=>$mediaUrl,
            "name"=>$user->info->name,
            "email"=>$user->email,
            "phone"=>(!empty($user->info->phone))?$user->info->phone:'',
            
        ]];
        return appResponse($data,'success');
   }

   function getProfile(Request $request)
   {
        $user_id        = $request->input('user_id');
        $user           = User::find($user_id);
        if(empty($user)) return appResponse($request, 'User Not Found');
        if ($user->hasRole('Employeer')) {
            $data = $this->userDetail($user);
            return appResponse($data,'success');
        }
        else{
            $data = $this->userDetailCompany($user);
            return appResponse($data,'success');
        }
   }

   function update(Request $request)
   {
        $user_id        = $request->input('user_id');
        $user           = User::find($user_id);
        $type           = $request->input('type');
        if(empty($user)) return appResponse($request, 'User Not Found');
        if($type == 'avatar') {
            $image = $request->file('image');

            try {
                $this->validate($request, [
                        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', //only allow this type extension file.
                    ]);
            } catch (ValidationException $e) {
                return appResponse($request,'forbidden',$e->getMessage());
            }
            if ($request->hasFile('image')) {
                $media=$user->info->getFirstMedia('avatar');
                if(!empty($media)) Media::find($media->id)->delete();
                $user->info->addMedia($image)->toMediaCollection('avatar');
                if ($user->hasRole('Employeer')) {
                    $data = $this->userDetail($user);
                }else{
                    $data = $this->userDetailCompany($user);
                }
                return appResponse($data,'success');
            }
        }
        if ($user->hasRole('Employeer')) {
            if($type == 'work_experiences') {
                $work_experiences = $request->input('work_experiences');
                $dataSet = [];
                foreach ($work_experiences as $data) {
                    $dataSet[] = [
                        'user_id'       => $user_id,
                        'job_title'    => $data['job_title'],
                        'company'    => $data['company'],
                        'job_desc'    => $data['job_desc'],
                        'start_date'    => $data['start_date'],
                        'end_date'    => $data['end_date'],
                    ];
                }   
                // Save User Work Experiences
                DB::table('user_work_experiences')->where('user_id',$user_id)->delete();
                DB::table('user_work_experiences')->insert($dataSet);
                $data = $this->userDetail($user);
                return appResponse($data,'success');
            }
            elseif ($type == 'profile') {
                $name           = $request->input('name');
                $email          = $request->input('email');
                $password       = $request->input('password');
                $phone          = $request->input('phone');
                $description    = $request->input('description');
                $dob            = $request->input('dob');
                //Check Parameter
                $checkParam=cparam($request,['name','email','phone']);
                if($checkParam!==true) return $checkParam;

                //update user
                if(!empty($email)) $user->email=$email;
                if(!empty($password)) $user->password=app('hash')->make($password);
                $user->save();

                //update user info
                if(!empty($name)) $user->info->name=$name;
                if(!empty($phone)) $user->info->phone=$phone;
                if(!empty($description)) $user->info->description=$description;
                if(!empty($dob)) $user->info->dob=$dob;
                $user->info->save();

                $data = $this->userDetail($user);
                return appResponse($data,'success');
            }
            elseif ($type == 'skills') {
                $skills = $request->input('skills');
                $dataSet = [];
                foreach ($skills as $data) {
                    $dataSet[] = [
                        'user_id'               => $user_id,
                        'title'             => $data['title'],
                    ];
                }   
                // Save User Work Experiences
                DB::table('user_skills')->where('user_id',$user_id)->delete();
                DB::table('user_skills')->insert($dataSet);
                $data = $this->userDetail($user);
                return appResponse($data,'success');
            }
            elseif ($type == 'educations') {
                $educations = $request->input('educations');
                $dataSet = [];
                foreach ($educations as $data) {
                    $dataSet[] = [
                        'user_id'               => $user_id,
                        'institute'             => $data['institute'],
                        'education_level_id'    => $data['education_level_id'],
                        'major'                 => $data['major'],
                        'graduation_year'       => $data['graduation_year'],
                        'gpa'                   => $data['gpa'],
                    ];
                }   
                // Save User Work Experiences
                DB::table('user_educations')->where('user_id',$user_id)->delete();
                DB::table('user_educations')->insert($dataSet);
                $data = $this->userDetail($user);
                return appResponse($data,'success');
            }
            elseif ($type == 'resume') {
                $resume = $request->file('resume');

                try {
                    $this->validate($request, [
                            'resume' => 'required|max:10000|mimes:doc,docx,pdf,jpg,jpeg', //only allow this type extension file.
                        ]);
                } catch (ValidationException $e) {
                    return appResponse($request,'forbidden',$e->getMessage());
                }
                if ($request->hasFile('resume')) {
                    $user_resume = $user->info->resume;
                    if(!empty($user_resume)) {
                        $ext = $resume->getClientOriginalExtension();
                        $newName = rand(100000,1001238912).".".$ext;
                        unlink('resume/'.$user->id.'/'.$user_resume);
                        $resume->move('resume/'.$user->id,$newName);
                        $user->info->resume = $newName;
                        $user->info->update();
                        $data = $this->userDetail($user);
                        return appResponse($data,'success');
                    }
                    else{   
                        $ext = $resume->getClientOriginalExtension();
                        $newName = rand(100000,1001238912).".".$ext;
                        $resume->move('resume/'.$user->id,$newName);
                        $user->info->resume = $newName;
                        $user->info->save();
                        $data = $this->userDetail($user);
                        return appResponse($data,'success');
                    }
                }
            }
        }
        elseif ($user->hasRole('Company')) {
            if($type == 'profile') {
                $name           = $request->input('name');
                $email          = $request->input('email');
                $password       = $request->input('password');
                $phone          = $request->input('phone');
                $description    = $request->input('description');
                $address        = $request->input('address');
                //Check Parameter
                $checkParam=cparam($request,['name','email','phone']);
                if($checkParam!==true) return $checkParam;

                //update user
                if(!empty($email)) $user->email=$email;
                if(!empty($password)) $user->password=app('hash')->make($password);
                $user->save();

                //update user info
                if(!empty($name)) $user->info->name=$name;
                if(!empty($phone)) $user->info->phone=$phone;
                if(!empty($description)) $user->info->description=$description;
                if(!empty($address)) $user->info->address=$address;
                $user->info->save();

                $data = $this->userDetailCompany($user);
                return appResponse($data,'success');
            }
        }
   }

   public static function userDetailCompany(User $user) 
   {
        $roles = $user->getRoleNames()->first();
        //Get Url
        $getMedia = Media::where('model_id',$user->info->id)->first();
        if($getMedia){
            $mediaUrl = env('APP_URL').'/media/'.$getMedia->id.'/'.$getMedia->file_name;
        }
        else{
             $mediaUrl = env('APP_URL').'/img/avatar.png';
        }
        return $data=[[
            "role"=>$roles,
            "avatar"=>$mediaUrl,
            "name"=>$user->info->name,
            "email"=>$user->email,
            "phone"=>(!empty($user->info->phone))?$user->info->phone:'',
            "description"=>(!empty($user->info->description))?$user->info->description:'',
            "address"=>(!empty($user->info->address))?$user->info->address:'',
            
        ]];
   }

   public static function userDetail(User $user)
   {
        $roles = $user->getRoleNames()->first();
        $work_experiences = $user->work_experiences;
        $skills = $user->skills;
        $educations = DB::table('user_educations')
                        ->leftJoin('education_level','user_educations.education_level_id','education_level.id')
                        ->where('user_educations.user_id',$user->id)
                        ->select(
                            'user_educations.institute',
                            'education_level.title',
                            'user_educations.major',
                            'user_educations.graduation_year',
                            'user_educations.gpa'
                        )->get();
        //Get Url
        $getMedia = Media::where('model_id',$user->info->id)->first();
        if($getMedia){
            $mediaUrl = env('APP_URL').'/media/'.$getMedia->id.'/'.$getMedia->file_name;
        }
        else{
             $mediaUrl = env('APP_URL').'/img/avatar.png';
        }
        if (@empty($user->info->resume)) {
            $resume = env('APP_URL').'/resume/'.$user->id.'/'.$user->info->resume;
        }else{
            $resume = "";
        }
        return $data=[[
            "role"=>$roles,
            "avatar"=>$mediaUrl,
            "resume"=>(!empty($resume))?$resume:'',
            "name"=>$user->info->name,
            "email"=>$user->email,
            "dob"=>(!empty($user->info->dob))?$user->info->dob:'',
            "phone"=>(!empty($user->info->phone))?$user->info->phone:'',
            "description"=>(!empty($user->info->description))?$user->info->description:'',
            "work_experiences"=>(!empty($work_experiences))?$work_experiences:'',
            "skills"=>(!empty($skills))?$skills:'',
            "educations"=>(!empty($educations))?$educations:'',
            
        ]];
   }
}
