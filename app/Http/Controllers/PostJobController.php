<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\PostJob;
use App\Models\PostJobDescription;
use App\Models\PostJobRequirement;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class PostJobController extends Controller
{
	public function getJobs(Request $request)
	{
		$user_id = $request->input('user_id');
		$today = Carbon::today()->toDateString();
		$postJobs = PostJob::where('user_id',$user_id)->where('closing_date','>=',$today)->get();
		$value=[];
		$i = 0;
		foreach ($postJobs as $postJob) {
			$value[$i]['id'] = $postJob->id;
			$value[$i]['job_title'] = $postJob->job_title;
			if ($postJob->start_salary != $postJob->end_salary) {
				$salary = 'IDR '.number_format($postJob->start_salary, 0).' - IDR '.number_format($postJob->end_salary, 0);
			}
			else{
				$salary = 'IDR '.number_format($postJob->start_salary, 0);
			}
			$value[$i]['salary'] = $salary;
			$value[$i]['closing_date'] = Carbon::parse($postJob->closing_date)->diffForHumans();
			$value[$i++];
		}
		$data=$value;
		return appResponse($data,'success');
	}
	public function postJobs(Request $request)
	{
		$user_id 			= $request->input('user_id');
		$job_title 			= $request->input('job_title');
		$start_salary 		= $request->input('start_salary');
		$end_salary 		= $request->input('end_salary');
		$employment_type 	= $request->input('employment_type');
		$closing_date 		= $request->input('closing_date');
		$post_job = new PostJob;
		$post_job->user_id 			= $user_id;
		$post_job->job_title 		= $job_title;
		$post_job->start_salary 	= $start_salary;
		$post_job->end_salary 		= (!empty($end_salary))?$end_salary:$start_salary;
		$post_job->employment_type 	= $employment_type;
		$post_job->closing_date 	= $closing_date;
		$post_job->status 			= 1001;
		$savePJ = $post_job->save();
		if ($savePJ) {
			$job_descriptions = $request->input('job_descriptions');
            $dataJobDesc = [];
            foreach ($job_descriptions as $jobdesc) {
                $dataJobDesc[] = [
                    'post_jobs_id'  => $post_job->id,
                    'sequence_no'   => $jobdesc['sequence_no'],
                    'title'     	=> $jobdesc['title'],
                ];
            }   
            // Save User Job Description
        	$savePJD = DB::table('post_job_descriptions')->insert($dataJobDesc);
        	if ($savePJD) {
        		$requirements = $request->input('requirements');
	            $dataRequirement = [];
	            foreach ($requirements as $data) {
	                $dataRequirement[] = [
	                    'post_jobs_id'  => $post_job->id,
	                    'sequence_no'   => $data['sequence_no'],
	                    'title'     	=> $data['title'],
	                ];
	            }   
	            // Save User Job Requirement
            	$savePJR = DB::table('post_job_requirements')->insert($dataRequirement);
            	if ($savePJR) {
            		$data = 'Job Already Save';
    				return appResponse($data,'success');
            	}else{
            		PostJob::find($post_job->id)->delete();
            		DB::table('post_job_descriptions')->where('post_jobs_id',$post_job->id)->delete();
    				return appResponse($request,'forbidden','Error in Save Requirement, Try Again!');
            	}
        	}
        	else{
        		PostJob::find($post_job->id)->delete();
				return appResponse($request,'forbidden','Error in Save Description, Try Again!');
        	}
		}
		else{
			return appResponse($request,'forbidden','Error in Save Job, Try Again!');
		}

	}
}