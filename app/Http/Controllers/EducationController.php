<?php

namespace App\Http\Controllers;

use App\Models\EducationLevel;

class EducationController extends Controller
{
	public function getEducationLevel()
	{
		$educationLevel = EducationLevel::all()->pluck('title');
		return appResponse($educationLevel,'success');
	}
}